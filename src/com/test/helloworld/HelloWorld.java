package com.test.helloworld;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.EditText;
import android.widget.TextView;

public class HelloWorld extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // loading the layout over the resource reference
        setContentView(R.layout.activity_main);
        
        // get the two controls we created earlier, also with the resource reference and the id
        final TextView tv_View = (TextView)findViewById(R.id.tv_View);
        final EditText et_Text = (EditText)findViewById(R.id.et_Text);
        
        // add new KeyListener Callback (to record key input)
        et_Text.setOnKeyListener(new OnKeyListener() {
            // function to invoke when a key is pressed
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // check if there is
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    // check if the right key was pressed
                    if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) { // DPAD is emulator only
                        // add the text to the textView
                        // Takes what was originally in the textView, appends the editText, and then re-sets the textView
                        tv_View.setText(tv_View.getText() + ", " + et_Text.getText());
                        // clear the EditText control
                        et_Text.setText("");
                        return true;
                    }
                }
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

}